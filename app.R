

# Load the libraries. I assume they are installed on the server.
library(shiny)
library(fresh)
library(tidyverse)
library(bs4Dash)
library(shinymanager)
library(shinyWidgets)
library(echarts4r)
library(rintrojs)
library(deSolve)
library(DT)
library(leaflet)
library(nasapower)



# Style for the DT Tables
dt_style <- c(
  "function(row, data, num, index){",
  "  var $row = $(row);",
  "  if($row.hasClass('even')){",
  "    $row.css('background-color', '#f8f9fa');",
  "    $row.hover(function(){",
  "      $(this).css('background-color', '#6c757de0');",
  "     }, function(){",
  "      $(this).css('background-color', '#f8f9fa');",
  "     }",
  "    );",
  "  }else{",
  "    $row.css('background-color', '#dee2e6');",
  "    $row.hover(function(){",
  "      $(this).css('background-color', '#6c757de0');",
  "     }, function(){",
  "      $(this).css('background-color', '#dee2e6');",
  "     }",
  "    );",
  "  }",
  "}"
)



# Load the data for the wind farms
stations = readr::read_csv("world_stations.csv")
DTable = function(df,
                  captions = NULL,
                  height = 400,
                  filter = 'none') {
  DT::datatable(
    df,
    caption = captions,
    class = 'cell-border stripe',
    filter = filter,
    height = height,
    extensions = c('Scroller', 'Buttons'),
    options = list(
      searching = FALSE,
      columnDefs = list(list(
        className = 'dt-center', targets = 1:ncol(df)
      )),
      selection = 'none',
      dom = 'Bfrtip',
      scrollY = height,
      scroller = TRUE,
      scrollX = 550,
      buttons = list(
        'copy',
        'print',
        list(
          extend = 'collection',
          buttons = c('csv', 'excel'),
          text = 'Download'
        )
      ),
      initComplete = htmlwidgets::JS(
        "function(settings, json) {",
        "$(this.api().table().header()).css({'background-color': '#293c55',
                                          'color': 'white'});",
        "}"
      )
    ),
    escape = FALSE,
    editable = FALSE
  )  %>% DT::formatStyle(columns = 0:ncol(df), border = '1px solid #000')
  
}




#======================================================================================================================#
#                                         Prepare the UI Theme ----
#======================================================================================================================#
bs4DashTheme <- create_theme(
  bs4dash_layout(main_bg = '#f8f9fa',
                 sidebar_width = "320px"),
  bs4dash_status(
    primary = '#293c55',
    danger = '#BF616A',
    light = '#272c30'
  ),
  bs4dash_sidebar_dark(
    bg = '#dee2e6',
    header_color = '#000',
    color = '#c2c7d0',
    hover_color = '#fff',
    hover_bg = '#5cc6c7'
  ),
  bs4dash_sidebar_light(
    bg = '#dee2e6',
    color = '#fff',
    header_color = '#000',
    hover_color = '#fff',
    hover_bg = '#293c55'
  ),
  bs4dash_font(weight_bold = 600,
               family_base = 'intercom-font')
)





#======================================================================================================================#
#                                             UI Code ----
#======================================================================================================================#

ui <- bs4DashPage(
  fullscreen = TRUE,
  scrollToTop = TRUE,
  title = '',
  header =    bs4DashNavbar(
    fixed = TRUE,
    title = dashboardBrand(
      color = 'secondary',
      title = "Solar Pond Simulator",
      image = NULL,
      opacity = 0.8
    ),
    actionButton(
      inputId = 'show_documentation',
      label = 'Documentation',
      icon = icon('book'),
      style = 'border-width:2px; border-color:black;'
    ),
    
    rightUi = tagList(
      tags$li(
        class = 'dropdown',
        style = 'margin-top: 10px',
        tags$a(
          href = '',
          icon('chrome'),
          '',
          target = '_blank',
          style = 'color: #001f3f;'
        ),
      ),
      dropdownMenu(
        badgeStatus = "info",
        type = "tasks",
        headerText = 'Application Progress',
        taskItem(
          inputId = "triggerAction0",
          text = "Project Completion",
          color = "orange",
          value = 65
        )
      ),
      userOutput("user_panel")
    )
    
  ),
  sidebar = bs4DashSidebar(
    disable = FALSE,
    width = 450,
    minified = FALSE,
    br(),
    sidebarMenu(
      id = "current_tab",
      flat = FALSE,
      compact = FALSE,
      childIndent = TRUE
    ),
    
    sidebarPanel(
      width = 12,
      fluidRow(
        h4("Initial Conditions", style = 'font-weight:bold;'),
        numericInput(
          inputId = 'Tu_initial',
          label =  "Initial Temperature of UCZ (\u00B0C)",
          value = 14
        ),
        numericInput(
          inputId = 'Ts_initial',
          label = 'Initial Temperature of LCZ (\u00B0C)',
          value = 23
        ),
        br(),
        h4("Pond Dimensions", style = 'font-weight:bold;'),
        numericInput(
          inputId = 'par_X_u',
          label = "Thickness of the UCZ (m)",
          value = 0.50
        ),
        numericInput(
          inputId = 'par_X_ncz',
          label = "Thickness of the NCZ (m)",
          value = 0.20
        ),
        numericInput(
          inputId = 'par_X_l',
          label = "Thickness of the LCZ (m)",
          value = 0.30
        ),
        numericInput(
          inputId = 'par_A_u',
          label = HTML(paste0(
            "Surface area of the UCZ (m", tags$sup("2"), ')'
          )),
          #"Surface area of the UCZ (m2)",
          value = 7.20
        ),
        numericInput(
          inputId = 'par_A_l',
          label =  HTML(paste0(
            "Surface area of the LCZ (m", tags$sup("2"), ')'
          )),
          value = 7.20
        ),
        numericInput(
          inputId = 'par_A_b',
          label =  HTML(paste0(
            "Bottom surface area (m", tags$sup("2"), ')'
          )),
          #"Bottom surface area (m2)",
          value = 7.20
        ),
        br(),
        br(),
        h4("Density Properties", style = 'font-weight:bold;'),
        numericInput(
          inputId = 'par_rho_u',
          label =  HTML(paste0(
            "Density of the UCZ (kg/m", tags$sup("3"), ')'
          )),
          value = 1000
        ),
        numericInput(
          inputId = 'par_rho_l',
          label =  HTML(paste0(
            "Density of the LCZ (kg/m", tags$sup("3"), ')'
          )),
          value = 1200
        ),
        br(),
        br(),
        h4("Heat Transfer Settings", style = 'font-weight:bold;'),
        numericInput(
          inputId = 'par_c_pu',
          label = "Heat capacity of water in the UCZ ",
          value = 4180
        ),
        numericInput(
          inputId = 'par_c_pl',
          label = "Heat capacity of water in the LCZ ",
          value = 3300
        ),
        numericInput(
          inputId = 'par_h1',
          label = HTML(
            paste0(
              "Heat transfer coefficient between the NCZ and the UCZ(W/m",
              tags$sup("2"),
              'K)'
            )
          ),
          value = 56.58
        ),
        numericInput(
          inputId = 'par_h2',
          label = HTML(
            paste0(
              "Heat transfer coefficient between the LCZ and the NCZ (W/m",
              tags$sup("2"),
              'K)'
            )
          ),
          value = 48.278
        ),
        numericInput(
          inputId = 'par_h3',
          label = "Heat transfer coefficient between the LCZ with surface at the bottom of the pond",
          value = 78.12
        ),
        numericInput(
          inputId = 'par_h4',
          label = "Heat transfer coefficient at the surface of the ground water sink",
          value = 185
        ),
        numericInput(
          inputId = 'par_Kw',
          label = "Thermal conductivity of water",
          value = 0.596
        ),
        numericInput(
          inputId = 'par_xg',
          label = "Distance of water table from pond’s bottom (m)",
          value = 1
        ),
        numericInput(
          inputId = 'par_Tg',
          label = "Temperature of water table under the pond (C)",
          value = 23
        ),
        numericInput(
          inputId = 'par_lambda',
          label = "Latent heat of vaporisation (kJ/kg)",
          value = 2458.3
        ),
        br(),
        h4("Other Parameters", style = 'font-weight:bold;'),
        # numericInput(
        #   inputId = 'par_p_atm',
        #   label = "Atmospheric pressure (mmHg)",
        #   value = 763
        # ),
        # numericInput(
        #   inputId = 'par_H',
        #   label = HTML(paste0("Solar radiation (W/m",tags$sup("2"), ')')),
        #   value = 825.12
        # ),
        numericInput(
          inputId = 'par_kg',
          label = "Thermal conductivity of the soil under the pond (W/m K)",
          value = 0.7
        ),
        # numericInput(
        #   inputId = 'par_Ta',
        #   label = "Average of the ambient temperature (°C)",
        #   value = 37.6
        # ),
        numericInput(
          inputId = 'par_Q_load',
          label = 'Q load',
          value = 0
        )
      )
      
    )
    
    
  ),
  body = dashboardBody(
    #shinyalert::useShinyalert(),
    tags$style(
      HTML(
        "
                                    div.sidebar .shiny-bound-input.action-button, div.sidebar .shiny-bound-input.action-link {
                                      margin: 6px 5px 6px 15px;
                                      display: block;
                                      width: 260px;
                                      margin-left: 60px;
                                    }

                                  .form-control {
                                      display: block;
                                      width: 100%;
                                      height: calc(2.25rem + 2px);
                                      padding: .375rem .75rem;
                                      font-size: 1rem;
                                      font-weight: 400;
                                      line-height: 1.5;
                                      color: #495057;
                                      background-color: #fff;
                                      background-clip: padding-box;
                                      border: 3px solid #ced4da;
                                      border-radius: .25rem;
                                      box-shadow: inset 0 0 0 rgb(0 0 0 / 0%);
                                      transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
                                  }

                                  label:not(.form-check-label):not(.custom-file-label) {
                                    font-weight: 600;
                                    font-family: system-ui;
                                  }


                                .element.style {
                                  text-align: center;
                                  font-size: 1rem;
                                }

                                  /*Modify the size of the modal window*/
                                  .modal-dialog {
                                    position: relative;
                                    width: 960px;
                                    pointer-events: none;
                                  }

                                /*Modify the size of the modal window*/
                                .modal-content {
                                    position: relative;
                                    display: flex;
                                    flex-direction: column;
                                    width: 105%;
                                    pointer-events: auto;
                                    background-color: #fff;
                                    background-clip: padding-box;
                                    border: 2px solid rgba(0,0,0,0.2);
                                    border-radius: .3rem;
                                    box-shadow: 0 0.25rem 0.5rem rgb(0 0 0 / 50%);
                                    outline: 0;
                                }
        "
      )
    ),
    
    use_theme(bs4DashTheme),
    
    tabItems(# First tab
      tabItem(
        tabName = "dashboard",
        class = "active",
        tabBox(
          selected = "Solar Pond Model",
          width = 12,
          id = 'tabbox',
          status = 'primary',
          collapsible = FALSE,
          tabPanel(title = "Solar Pond Model",
                   fluidRow(
                     box(
                       solidHeader = TRUE,
                       status = 'primary',
                       width = 4,
                       collapsible = FALSE,
                       selectInput(
                         inputId = "data_freq",
                         label = 'Temporal Resolution',
                         choices = c('hourly', 'daily', 'monthly'),
                         selected = 'hourly'
                       ),
                       dateRangeInput(
                         inputId = "start_end_date",
                         label = "Date Range",
                         start = '2014-01-01',
                         end = '2014-03-01'
                       ),
                       selectInput(
                         inputId = 'location_specification',
                         label = 'Location Settings',
                         choices = c('Manual', 'Use Map'),
                         selected = 'Manual'
                       ),
                       numericInput(
                         inputId = "max_lcz_temperature",
                         label = "Max LCZ Temperature (°C)",
                         value = 90,
                         step = 5
                       ),
                       conditionalPanel(
                         condition = "input.location_specification == 'Manual'",
                         splitLayout(
                           cellWidths = c('50%', '50%'),
                           numericInput(
                             inputId = 'pick_latitude',
                             label = 'Latitude',
                             value = 29.55767,
                             min = -90,
                             max = 90
                           ),
                           numericInput(
                             inputId = 'pick_longitude',
                             label = 'Longitude',
                             value = 34.95193,
                             min = -180,
                             max = 180
                           )
                         )
                       ),
                       actionButton(
                         inputId = "pull_nasa_data",
                         label = 'Download Data',
                         width = '100%',
                         icon = icon('download'),
                         style = 'border-width:2px;border-color:black;background:#909ba7;'
                       ),
                       br(),
                       br(),
                       uiOutput(outputId = 'run_simulations_ui'),
                       br(),
                       conditionalPanel(
                         condition = "input.location_specification == 'Manual'",
                         shinycssloaders::withSpinner(leafletOutput(outputId = 'picked_point_map'), type = 8)
                       ),
                       conditionalPanel(
                         condition = "input.location_specification != 'Manual'",
                         box(
                           width = 12,
                           collapsible = FALSE,
                           maximizable = TRUE,
                           status = 'primary',
                           solidHeader = TRUE,
                           title = 'Move market to specify location',
                           leaflet::leafletOutput('base_map')
                         )
                       )
                     ),
                     
                     box(
                       width = 8,
                       maximizable = FALSE,
                       collapsible = FALSE,
                       solidHeader = FALSE,
                       box(
                         width = 12,
                         collapsible = TRUE,
                         maximizable = TRUE,
                         status = 'primary',
                         solidHeader = TRUE,
                         title = 'Results',
                         id = 'nasa_data_box',
                         # conditionalPanel(
                         #   condition = "input.location_specification != 'Manual'",
                         #   box(
                         #     width = 12,
                         #     collapsible = TRUE,
                         #     maximizable = TRUE,
                         #     status = 'primary',
                         #     solidHeader = TRUE,
                         #     title = 'Specify Location',
                         #     leaflet::leafletOutput('base_map')
                         #   )
                         # ),
                         shinycssloaders::withSpinner(plotOutput(outputId = 'nasa_data_fig', height = 620), type = 8),
                         shinycssloaders::withSpinner(DT::dataTableOutput(outputId = 'nasa_data_tbl'), type = 8)
                         
                         
                         
                       ),
                       br(),
                       box(
                         width = 12,
                         status = 'primary',
                         solidHeader = TRUE,
                         title = 'Solve System with Real-World Data',
                         maximizable = TRUE,
                         checkboxInput(inputId = 'interactive_static_figure', label = 'Interactive Figure'),
                         conditionalPanel(
                           condition = "input.interactive_static_figure == true",
                           shinycssloaders::withSpinner(
                             echarts4r::echarts4rOutput(outputId = 'system_output_fig_inter'),
                             type = 8
                           )
                         ),
                         conditionalPanel(
                           condition = "input.interactive_static_figure == false",
                           shinycssloaders::withSpinner(plotOutput(outputId = 'system_output_fig'), type = 8)
                         ),
                         br(),
                         DT::dataTableOutput(outputId = 'system_output_tbl')
                       )
                       
                     )
                     
                   )),
          tabPanel(
            title = 'Weather Stations',
            checkboxInput(
              inputId = 'show_heatmap',
              label = 'Heatmap',
              value = FALSE
            ),
            shinycssloaders::withSpinner(
              leaflet::leafletOutput(outputId = 'leaflet_map', height = 650),
              type = 8
            ),
            br(),
            br(),
            DT::dataTableOutput(outputId = 'weather_stations_tbl')
          )
        )
      ))
    
  )
  
  
  
)



#======================================================================================================================#
#                                             Server Code ----
#======================================================================================================================#
server <- function(input, output, session) {
  
  observeEvent(input$solve_new_system, {
    updateBox("nasa_data_box", action = "toggle")
  })
  
  observeEvent(input$pull_nasa_data, {
    updateBox("nasa_data_box", action = "toggle")
  })
  
  
  #============================================#
  #   Documentation ----
  #============================================#
  observeEvent(input$show_documentation, {
    showModal(
      modalDialog(
        footer =  modalButton(label = 'Close'),
        size = 'l',
        easyClose = TRUE,
        title = "DOCUMENTATION PLACEHOLDER",
        div(
          id = 'documentation_Box',
          style = 'overflow-y:scroll; height: 520px;',
          h4('1. Objective',  style = 'font-weight: bold;'),
          p(
            "The goal of the application is to allow users to specify any location on the planet and run solar pond simulations.
            Solar ponds are proving to collect and store solar energy.
            "
          ),
          h4('2. Data Sources',  style = 'font-weight: bold;'),
          p(
            "In order to solve the system of differential equations there are many meteorological parameters that need to be set. To achieve this,
            we rely on NASA's Prediction of Worldwide Energy Resources (POWER)."
          ),
          h4('3. Underlying Model',  style = 'font-weight: bold;')
          
        )
      )
    )
    
    
  })
  
  
  
  
  SolarPondModel = function(t, xx, parms) {
    Tu = xx[1]
    Ts = xx[2]
    
    with(as.list(parms), {
      # Equation 1 - Lower Convective Zone
      dTu = (A_u * (
        (H * (1 - 0.36 + 0.08 * log(X_u))) + (Ts - Tu) / (1 / h1  + 1 / h2 + X_ncz / Kw) -  (5.70 + 3.80 * v) *
          (Tu - Ta) - 4.708 * 10 ^ -8 * (Tu ^ 4 - (0.0552 * ifelse(
            Ta <= 0, abs(Ta), Ta
          ) ^ 1.5) ^ 4)  -
          lambda * (5.7 + 3.80 * v) * ((exp(
            18.403 - 3885 / (Tu + 230)
          )) - (gamma_h * exp(
            18.403 - 3885 / (Ta + 230)
          ))) / (1.6 * (1.005 + 1.82 * gamma_h) * p_atm)
      )) / (rho_u * c_pu * A_u * X_u)
      
      
      # Equation 2 - Upper Convective Zone
      dTs = (A_l * ((H * (
        0.36 - 0.08 * log(X_u + X_ncz)
      )) - (Ts - Tu) / (1 / h1 + 1 / h2 + X_ncz / Kw) - Q_load) - (A_b * (Ts - Tg)) /
        (1 / h3 + 1 / h4 + xg / kg)) / (rho_l * c_pl * A_l * X_l)
      
      list(c(dTu, dTs))
      
    })
  }
  
  
  
  # Create and display the map of world weather stations
  output$leaflet_map <- renderLeaflet({
    weather_stations <- stations
    
    # Prepare map labels
    labels <- sprintf("<strong> Station Name: %s </strong>",
                      weather_stations$id) %>%
      lapply(HTML)
    
    
    map <-
      leaflet(weather_stations) %>%
      addProviderTiles("CartoDB.DarkMatter",
                       options = providerTileOptions(minZoom = 1, maxZoom = 14))  %>%
      addMarkers(
        ~ lon,
        ~ lat,
        label = ~ labels,
        clusterOptions = markerClusterOptions(),
        labelOptions = labelOptions(
          noHide = FALSE,
          direction = "bottom",
          style = list(
            "color" = "black",
            "font-family" = "serif",
            "font-style" = "italic",
            "box-shadow" = "3px 3px rgba(0,0,0,0.25)",
            "font-size" = "14px",
            "border-color" = "rgba(0,0,0,0.5)"
          )
        )
      ) %>%
      addMiniMap(tiles = providers$Esri.WorldStreetMap,
                 toggleDisplay = TRUE)
    
    if (input$show_heatmap == TRUE) {
      map =
        map %>%
        leaflet.extras::addHeatmap(
          max = 10,
          group = 'dropoff',
          radius = 20,
          blur = 10
        )
    }
    
    return(map)
    
    
  })
  
  
  output$weather_stations_tbl = DT::renderDataTable({
    weather_stations <- stations
    
    DTable(weather_stations, filter = 'top')
    
  })
  
  
  
  #=============================#
  # NASA's dataset ----
  #=============================#
  
  nasa_datasets = eventReactive(input$pull_nasa_data, {
    if (input$location_specification == 'Manual') {
      loc = c(input$pick_longitude, input$pick_latitude)
    } else{
      loc = c(input$base_map_marker_dragend$lng,
              input$base_map_marker_dragend$lat)
    }
    
    
    nasa_data <- get_power(
      community = "sb",
      lonlat = loc,
      pars = c("RH2M", "T2M", 'WS2M', 'PS', 'ALLSKY_SFC_SW_DNI'),
      dates = c(input$start_end_date[1], to = input$start_end_date[2]),
      temporal_api = ifelse(input$data_freq == 'monthly', 'daily', input$data_freq)
    )
    
    
    
    showNotification(ui = 'Pulling NASA weather data...',
                     duration = 3,
                     type = 'message')
    if (input$data_freq == 'daily') {
      nasa_data$timestamp = lubridate::ymd(nasa_data$YYYYMMDD)
    } else if (input$data_freq == 'hourly') {
      nasa_data$timestamp = paste0(
        nasa_data$YEAR,
        '-',
        nasa_data$MM,
        '-',
        nasa_data$DD,
        ' ',
        nasa_data$HR,
        ':00:00'
      )
      
      nasa_data$timestamp = lubridate::ymd_hms(nasa_data$timestamp)
      
    } else if (input$data_freq == 'monthly') {
      nasa_data = nasa_data %>%
        dplyr::group_by(YEAR, MM) %>%
        summarise(
          RH2M = mean(RH2M),
          T2M  = mean(T2M),
          WS2M = mean(WS2M),
          ALLSKY_SFC_SW_DNI = mean(ALLSKY_SFC_SW_DNI),
          PS = mean(PS)
        ) %>% ungroup() %>%
        mutate(timestamp = paste0(YEAR, '-', MM, '-01'))
      
      nasa_data$timestamp = lubridate::ymd(nasa_data$timestamp)
    }
    
    
    return(nasa_data)
    
    
  })
  
  
  
  
  
  output$nasa_data_tbl = DT::renderDataTable({
    req(nasa_datasets())
    
    nasa_datasets() %>%
      mutate_if(is.numeric, round, 3) %>%
      rename(
        'Relative Humidity (2m)' = 'RH2M',
        'Temperature at 2m' = 'T2M',
        'Wind Speed at 2m' = 'WS2M',
        'All Sky Surface Shortwave Downward Direct Normal Irradiance W/m2' = 'ALLSKY_SFC_SW_DNI',
        'Surface Pressure' = 'PS'
      ) %>%
      dplyr::select(timestamp, dplyr::everything()) %>%
      DTable(height = 320)
  })
  
  
  
  
  output$nasa_data_fig = renderPlot({
    req(nasa_datasets())
    df_tmp = nasa_datasets()
    
    
    df_tmp$SOLAR_IRRADIANCE = df_tmp$ALLSKY_SFC_SW_DNI #df_tmp$ALLSKY_SFC_SW_DWN * 30 # per month
    
    
    df = df_tmp %>%
      dplyr::select(timestamp, RH2M, T2M, WS2M, SOLAR_IRRADIANCE, PS) %>%
      tidyr::gather(key = 'param', value = 'measurement', -timestamp)
    
    df = df %>% mutate(
      param = case_when(
        param == 'RH2M' ~ 'Relative Humidity %',
        param == 'T2M'  ~ '°C Temperature at 2m',
        param == 'WS2M' ~ 'Wind Speed at 2m (m/s)',
        param == 'SOLAR_IRRADIANCE' ~ 'All Sky Surface Shortwave Downward Irradiance W/m^2',
        param == 'PS' ~ 'Surface Pressure (kPa)'
      )
    )
    
    
    df %>%
      ggplot(aes(timestamp, measurement)) +
      geom_line() +
      geom_smooth(method = 'loess',
                  se = FALSE,
                  col = 'orange') +
      facet_wrap( ~ param, scales = 'free', ncol = 1) +
      theme_gray(base_size = 16) +
      theme(
        axis.line = element_line(colour = 'black', size = 1),
        strip.background = element_rect(fill = 'darkblue'),
        strip.text = element_text(colour = 'white')
      )
  })
  
  
  output$base_map = renderLeaflet({
    leaflet() %>%
      setView(lng = -0.121,
              lat = 51.502,
              zoom = 3) %>%
      addTiles() %>%
      addMarkers(
        lng = -0.121,
        lat = 51.502,
        popup = "Origin Point",
        options = markerOptions(draggable = TRUE)
      )
    
  })
  
  
  output$picked_point_map = renderLeaflet({
    req(input$pick_longitude)
    
    leaflet() %>%
      setView(
        lng = input$pick_longitude,
        lat = input$pick_latitude,
        zoom = 3
      ) %>%
      addTiles() %>%
      addMarkers(
        lng = input$pick_longitude,
        lat = input$pick_latitude,
        popup = "Selected Point",
        options = markerOptions(draggable = FALSE)
      )
    
  })
  
  
  
  output$run_simulations_ui = renderUI({
    req(nasa_datasets())
    actionButton(
      inputId = 'solve_new_system',
      label = 'Solve',
      icon = icon('cog'),
      width = '100%',
      style = 'border-width:2px;border-color:black;background:#909ba7;'
    )
    
  })
  
  
  solution_using_nasa_data = eventReactive(input$solve_new_system, {
    req(nasa_datasets())
    meteo_df = nasa_datasets()
    
    
    meteo_df$SOLAR_IRRADIANCE =  meteo_df$ALLSKY_SFC_SW_DNI
    
    if (input$data_freq == 'hourly') {
      times = seq(from = 0,
                  to = 3600,
                  by = 100)
    } else if (input$data_freq == 'daily') {
      times = seq(from = 0,
                  to = 3600 * 24,
                  by = 1000)
    } else if (input$data_freq == 'monthly') {
      times = seq(from = 0,
                  to = 3600 * 24 * 30,
                  by = 10000)
    }
    
    
    parms = c(
      X_u = input$par_X_u,
      X_ncz = input$par_X_ncz,
      X_l = input$par_X_l,
      rho_u = input$par_rho_u,
      rho_l = input$par_rho_l,
      c_pu = input$par_c_pu,
      c_pl = input$par_c_pl,
      A_u = input$par_A_u,
      A_l = input$par_A_l,
      A_b = input$par_A_b,
      h1 = input$par_h1,
      h2 = input$par_h2,
      h3 = input$par_h3,
      h4 = input$par_h4,
      Kw = input$par_Kw,
      xg = input$par_xg,
      Tg = input$par_Tg,
      lambda = input$par_lambda,
      p_atm = NA,
      v = NA,
      Q_load = input$par_Q_load,
      H = NA,
      Ta = NA,
      kg = input$par_kg,
      gamma_h = NA
    )
    
    
    params_matrix = as.data.frame(matrix(
      rep(parms, nrow(meteo_df)),
      ncol = length(parms),
      byrow = TRUE
    ))
    
    
    
    colnames(params_matrix) = names(parms)
    params_matrix$v       = meteo_df$WS2M #Overwrite with real-world wind speed at 2m height
    params_matrix$Ta      = meteo_df$T2M  # Overwrite with real-world air temperature at 2m height
    params_matrix$gamma_h = meteo_df$RH2M # Overwrite with real-world humidity
    params_matrix$H       = meteo_df$SOLAR_IRRADIANCE
    params_matrix$p_atm   = 7.501 * meteo_df$PS   # Scale to mmHg pressure
    
    
    
    # Initial conditions. Used only for iteration 1.
    xstart = c(Tu = input$Tu_initial, Ts = input$Ts_initial)
    
    outputs = list()
    for (i in 1:nrow(params_matrix)) {
      if (i == 1) {
        xstart = c(Tu = input$Tu_initial, Ts = input$Ts_initial)
      } else{
        xstart = c(Tu = last(outputs[[i - 1]][["Tu"]]), Ts = last(outputs[[i - 1]][["Ts"]]))
      }
      
      showNotification(paste0("Solving for ", meteo_df$timestamp[i]), duration = 1)
      
      sol = lsoda(
        xstart,
        times = times,
        func = SolarPondModel,
        parms = params_matrix[i, ],
        maxsteps = 300
      )
      
      outputs[[i]] = data.frame(
        timestamp = meteo_df$timestamp[i],
        t  = sol[, 1],
        Tu = sol[, 2],
        Ts = sol[, 3],
        wind_m_s = params_matrix$v[i],
        temperature_celsius = params_matrix$Ta[i],
        solar_irradiance_w_m2 = params_matrix$H[i],
        relative_humidity = params_matrix$gamma_h[i],
        atm_pressure =  params_matrix$p_atm[i]
      )
      
    }
    
    
    names(outputs) = paste0("it_", 1:length(outputs))
    if (input$data_freq == 'hourly') {
      outputs_df = dplyr::bind_rows(outputs, .id = 'iter') %>% dplyr::filter(t == 1800)
    } else if (input$data_freq == 'daily') {
      outputs_df = dplyr::bind_rows(outputs, .id = 'iter') %>% dplyr::filter(t == 43000)
    } else if (input$data_freq == 'monthly') {
      outputs_df = dplyr::bind_rows(outputs, .id = 'iter') %>% dplyr::filter(t == 1290000)
    }
    
    
    
    
    return(outputs_df)
    
  })
  
  
  
  
  output$system_output_tbl = DT::renderDataTable({
    req(solution_using_nasa_data())
    sol_df = solution_using_nasa_data()
    
    sol_df$Ts = ifelse(sol_df$Ts > input$max_lcz_temperature,
                       input$max_lcz_temperature,
                       sol_df$Ts)
    sol_df = sol_df %>% rename(
      'Temperature of the LCZ (° C) - Ts' = 'Ts',
      'Temperature of the UCZ (° C) - Tu' = 'Tu'
    )
    
    sol_df %>%
      mutate_if(is.numeric, round, 4) %>%
      DTable(height = 340)
    
  })
  
  
  
  
  output$system_output_fig = renderPlot({
    req(solution_using_nasa_data())
    sol_df = solution_using_nasa_data()
    sol_df = sol_df %>% tidyr::gather(key = 'Variable', value = 'Value', Tu:Ts)
    
    sol_df = sol_df %>% mutate(
      Variable = case_when(
        Variable == 'Ts' ~ 'Temperature of the LCZ (° C) - Ts',
        Variable == 'Tu' ~ 'Temperature of the UCZ (° C) - Tu'
      ),
      Value = case_when(
        Variable == 'Temperature of the LCZ (° C) - Ts' & Value > 90 ~ 90,
        TRUE ~ Value
      )
    )
    
    sol_df %>%
      ggplot(aes(timestamp, Value)) +
      geom_line() +
      geom_smooth(method = 'loess',
                  col = 'orange',
                  se = FALSE) +
      facet_wrap(~ Variable, ncol = 1, scales = 'free') +
      theme_gray(base_size = 15) +
      theme(
        axis.line = element_line(colour = 'black', size = 1),
        strip.background = element_rect(fill = 'darkblue'),
        strip.text = element_text(colour = 'white'),
        legend.position = 'none'
      ) + ylab("Temperature (°C)") +
      ggsci::scale_color_jama()
    
    
  })
  
  
  
  output$system_output_fig_inter = echarts4r::renderEcharts4r({
    req(solution_using_nasa_data())
    sol_df = solution_using_nasa_data()
    sol_df = sol_df %>% tidyr::gather(key = 'Variable', value = 'Value', Tu:Ts)
    
    
    sol_df = sol_df %>% mutate(
      Variable = case_when(
        Variable == 'Ts' ~ 'Temperature of the LCZ (° C) - Ts',
        Variable == 'Tu' ~ 'Temperature of the UCZ (° C) - Tu',
        TRUE ~ Variable
      )
    )
    
    sol_df$Value = ifelse(
      sol_df$Value > input$max_lcz_temperature,
      input$max_lcz_temperature,
      sol_df$Value
    )
    
    
    sol_df %>%
      group_by(Variable) %>%
      arrange(timestamp) %>%
      e_charts(timestamp) %>%
      e_line(Value, legend = TRUE, symbol = 'none') %>%
      e_datazoom(type = 'slider', toolbox = TRUE) %>%
      e_theme('roma') %>%
      e_title("Temperature") %>%
      e_x_axis(timestamp, axisPointer = list(show = TRUE)) %>%
      e_axis_labels(x = '', y = '') %>%
      e_tooltip(trigger = 'axis') %>%
      e_text_style(fontSize = 16,
                   color = 'black',
                   fontFamily = 'Arial') %>%
      e_legend(orientation = 'vertical', top = 20)
    
    
    
  })
  
}


shiny::shinyApp(ui, server)